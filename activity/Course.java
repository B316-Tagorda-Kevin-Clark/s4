package com.zuitt.activity;


public class Course {

    private String name;

    private String description;
    private int seats;

    private double fee;
//
//    private Date startDate;
//
//    private Date endDate;

    private String instructor;

    public Course() {
    }
    //, Date startDate, Date endDate
    public Course(String name, String description, int seats, double fee, String instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.instructor = instructor;
//        this.startDate = startDate;
//        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

//    public Date getStartDate() {
//        return startDate;
//    }
//    public void setStartDate(Date startDate) {
//        this.startDate = startDate;
//    }
//
//    public Date getEndDate() {
//        return endDate;
//    }
//    public void setEndDate(Date endDate) {
//        this.endDate = endDate;
//    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }


    public void call(){
        System.out.println("Welcome to the course " + this.name + "." + " This course can be describe as " + this.description + "." + " Your instructor for this course is Sir " + "." + this.instructor + " Enjoy!");
    }
}
