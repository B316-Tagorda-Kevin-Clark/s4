package com.zuitt.activity;

public class User {

    private String name;
    private int age;

    private String email;

    private String address;


    public User() {
    }

    public User(String name, int age, String email, String address) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setName(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void call(){
        System.out.println("Hi! I'm " + this.name + "." + "I'm " + this.age + "." + "You can reach me via my email: " + this.email + "." + " When I'm off work, I can be found playing at my house in " + this.address + ".");
    }

}
