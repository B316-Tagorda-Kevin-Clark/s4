package com.zuitt.example;

public class Dog extends Animal{
    //"extends" keyword allows us to have this class inherit the attributes and methods of the Animal class
    //Parent class is the class where we inherit from
    //Child class/subclass a class that inherits from a parent
    //No, sub-class cannot inherit from multiple parents
    //Instead sub-class can "multiple inherit" from what we call interfaces

    private String dogBreed;

    public Dog(){
        super();//super is a reference to the parent class. super() is us accessing the constructor method of the parent class
        this.dogBreed = "Shihtzu";

    }

    public Dog(String name, String color, String breed){
        super(name,color);
        this.dogBreed = breed;

    }

    public String getDogBreed(){
        return dogBreed;
    }

    public void setDogBreed(String dogBreed){
        this.dogBreed = dogBreed;
    }

    public void greet(){
        super.call();//super is a reference to the parent class. super() is us accessing the constructor method of the animal class
        System.out.println("Bark!");
    }
}
