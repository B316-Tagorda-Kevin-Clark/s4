package com.zuitt.example;

import javax.swing.plaf.synth.SynthTextAreaUI;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//
        Car car2 = new Car();
//        car2.make = "Zenix";
//        car2.brand = "Toyota";
//        car2.price = 1999999;
//
        Car car3 = new Car();
//        car3.make = "Civic";
//        car3.brand = "Honda";
//        car3.price = 200000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);

        Driver driver1 = new Driver("Alejandro", 25);
//        System.out.println(driver1.name);

        car1.start();
        car2.start();
        car3.start();

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());


        //property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Innova");
        System.out.println(car2.getMake());

        //carDriver getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver ("Antonio", 21);
        car1.setCarDriver(newDriver);

        // Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        /*
        Mini Activity
        *Create new class called Animal with the following attributes
        * name - string
        * color -string
        *add constructors, getters and setters for the class
         */


        Animal animal1 = new Animal("Ginger", "Brown");
        animal1.call();


        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();

        dog1.setName("LunaFreya");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Black");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Aussie", "Choco Brown", "Shihtzu");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();

    }
}
