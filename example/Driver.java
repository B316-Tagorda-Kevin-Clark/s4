package com.zuitt.example;

public class Driver {
    private String name;
    private int age;

    public Driver(){

    }
    public Driver(String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getAge(){
        return age;
    }
    public void setName(int age){
        this.age = age;
    }
}
